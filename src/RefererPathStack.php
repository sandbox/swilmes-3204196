<?php

namespace Drupal\contextual_filter_referer;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Represents the current path for the referring request making the AJAX call.
 */
class RefererPathStack {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new CurrentPathStack instance.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * Returns the path of the referring request.
   *
   * @return string
   *   Returns the path, without leading slashes.
   */
  public function getRefererPath() {

    // Get the referer from the request headers.
    $referer = $GLOBALS['request']->headers->get('referer');

    // Trim the base path and query params.
    $path = str_replace(\Drupal::request()->getSchemeAndHttpHost(), '', $referer);
    $path = reset(explode('?', $path));

    return $path;
  }

}
